-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04. Nov, 2017 17:39 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skierlogs`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `ID` varchar(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `county` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(10) NOT NULL,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(60) NOT NULL,
  `yearOfBirth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skis`
--

CREATE TABLE `skis` (
  `skUserName` varchar(10) NOT NULL,
  `cID` varchar(10) NOT NULL,
  `sFallYear` year(4) NOT NULL,
  `skiDate` date NOT NULL,
  `skiArea` varchar(60) NOT NULL,
  `skiDistance` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `totdist`
--

CREATE TABLE `totdist` (
  `skUserName` varchar(10) NOT NULL,
  `sFallYear` year(4) NOT NULL,
  `totdist` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skis`
--
ALTER TABLE `skis`
  ADD PRIMARY KEY (`skUserName`,`cID`,`sFallYear`),
  ADD KEY `cID` (`cID`),
  ADD KEY `sFallYear` (`sFallYear`);

--
-- Indexes for table `totdist`
--
ALTER TABLE `totdist`
  ADD PRIMARY KEY (`skUserName`,`sFallYear`),
  ADD KEY `sFallYear` (`sFallYear`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skis`
--
ALTER TABLE `skis`
  ADD CONSTRAINT `skis_ibfk_1` FOREIGN KEY (`skUserName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `skis_ibfk_2` FOREIGN KEY (`cID`) REFERENCES `club` (`ID`),
  ADD CONSTRAINT `skis_ibfk_3` FOREIGN KEY (`sFallYear`) REFERENCES `season` (`fallYear`);

--
-- Begrensninger for tabell `totdist`
--
ALTER TABLE `totdist`
  ADD CONSTRAINT `totdist_ibfk_1` FOREIGN KEY (`skUserName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `totdist_ibfk_2` FOREIGN KEY (`sFallYear`) REFERENCES `season` (`fallYear`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
