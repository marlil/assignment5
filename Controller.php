<?php
include_once("Model.php");
include_once("View.php");


class Controller {
	public
		$db = null,
		$xml = null,
		$view = null;
	public function __construct() {  
		$this->db = new DBModel();					// Creating an object working on DB
		$this->view = new View();					// Creating an object for the view
	}
	public function invoke() {
		$this->db->importSkiers();	// Retreiving skiers from XML
		$this->db->importClubs();	// Retreiving clubs from XML
		$this->db->importSeasons();// Retreiving seasons from XML
		$this->db->importSkis();
		$this->db->importTotdis();
	}

}

?>
