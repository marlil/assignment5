<?php

Interface IModel {

public function getNode($el, $tag);

public function importSkiers();

public function importClubs();

public function importSeasons();

public function importSkis();

public function importTotdist();
}
?>
