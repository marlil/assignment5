<?php

include_once("IModel.php");



class DBModel implements IModel {

  protected $db = null;

  public function __construct($db = null){
	$this->xml = new DOMDocument;
	$this->xml->load('SkierLogs.xml');
    if($db){
      $this->db = $db;
    }
    else{
		try{    
			$this->db = new PDO('mysql:host=localhost;dbname=skierlogs', 'root', '');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e){
           echo $e->getMessage();
		}
    }
  }
 
 public function getNode($el, $tag){
    return $el->getElementsByTagName($tag)->item(0)->textContent;
  }

  public function importSkiers(){
	$xpath = new DOMXpath($this->xml);
	$skiers = $xpath->query("//SkierLogs/Skiers/Skier");
    if (!is_null($skiers)){
      foreach ($skiers as $skier) {
        $stmt = $this->db->prepare("INSERT INTO skier(userName, firstName, lastName, yearOfBirth) VALUES(?,?,?,?)");
		$stmt->bindValue(1, $skier->getAttribute('userName'), PDO::PARAM_STR);
		$stmt->bindValue(2, getNode($skier, 'FirstName'), PDO::PARAM_STR);
		$stmt->bindValue(3, getNode($skier, 'LastName'), PDO::PARAM_STR);
		$stmt->bindValue(4, getNode($skier, 'YearOfBirth'), PDO::PARAM_INT);
		$stmt->execute();
      }
    }
  }

  public function importClubs(){
	$xpath = new DOMXpath($this->xml);
	$clubs = $xpath->query("//SkierLogs/Clubs/Club");
    if (!is_null($clubs)){
      foreach ($clubs as $club) {
        $stmt = $this->db->prepare("INSERT INTO club(ID, name, city, county) VALUES (?,?,?,?)");
        $stmt->bindValue(1, $club->getAttribute('id'), PDO::PARAM_STR);
		$stmt->bindValue(2, getNode($club, 'Name'), PDO::PARAM_STR);
		$stmt->bindValue(3, getNode($club, 'City'), PDO::PARAM_STR);
		$stmt->bindValue(4, getNode($club, 'County'), PDO::PARAM_STR);
		$stmt->execute();
       }
    }
  }

  public function importSeasons(){
	$xpath = new DOMXpath($this->xml);
	$seasons = $xpath->query("//SkierLogs/Season");
    if(!is_null($seasons)){
      foreach ($seasons as $season) {
        $stmt = $this->db->prepare("INSERT INTO season(fallYear) VALUES (?)");
        $stmt->bindValue(1, $season->getAttribute('fallYear'), PDO::PARAM_INT);
        $stmt->execute();
      }
    }
  }

  public function importSkis(){
	$xpath = new DOMXpath($this->xml);
	$skis = $xpath->query("//SkierLogs/Season");
    if(!is_null($skis)){
      foreach ($skis as $ski) {
        $stmt = $this->db->prepare("INSERT INTO skis(skUserName, cID, sFallYear, skiDate, skiArea, skiDistance) VALUES(?,?,?,?,?,?)");
		$stmt->bindValue(3, $ski->getAttribute('fallYear'), PDO::PARAM_INT);
		$skiers = $xpath->query("//SkierLogs/Season/Skiers");
		foreach ($skiers as $club){
			$stmt->bindValue(2, $club->getAttribute('clubId'), PDO::PARAM_STR);
			$skier = $xpath->query("//SkierLogs/Season/Skiers/Skier");
			foreach ($skier as $skier){
				$stmt->bindValue(1, $skier->getAttribute('userName'), PDO::PARAM_STR);
				$log = $xpath->query("//SkierLogs/Season/Skiers/Skier/Log");
				foreach ($log as $entry){
					$stmt->bindValue(4, getNode($entry, 'Date'), PDO::PARAM_INT);
					$stmt->bindValue(5, getNode($entry, 'Area'), PDO::PARAM_STR);
					$stmt->bindValue(6, getNode($entry, 'Distance'), PDO::PARAM_INT);
				}
			}		
		}
		$stmt->execute();
	  }
	}
  }

  public function importTotdist(){
	$xpath = new DOMXpath($this->xml);
	$season = $xpath->query("//SkierLogs/Season");
    if(!is_null($season)){
		foreach ($season as $fallyear){
			$stmt = $this->db->prepare("INSERT INTO totdist(skUserName, sFallYear, totdist) VALUES(?,?,?)");
			$stmt->bindValue(2, $fallyear->getAttribute('fallYear')->textContent, PDO::PARAM_INT);
			$skiers = $xpath->query("//SkierLogs/Season/Skiers/Skier");
			foreach ($skiers as $skier){
				$totdis = 0;
				$stmt->bindValue(1, $skier->getAttribute('userName')->textContent, PDO::PARAM_STR);
				$log = $xpath->query("//SkierLogs/Season/Skiers/Skier/Log");
				foreach ($log as $entry){
					$totdis += getNode($entry, 'Distance');
				}
				$stmt->bindValue(3, $totdis, PDO::PARAM_INT);
			}
			$stmt->execute();
		}
    }
  }
}

?>
